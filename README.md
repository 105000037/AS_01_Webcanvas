# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | N         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 
這是整個網頁
 

左邊的部分是工具列包含:
pencil, earaser, clear ,text, redo, undo, and upload image
左下方四個小雞圖案是代表著各個shape brush:
Line triangle circle rectangle
再往下就是調整brush大小及顏色
而左上方有一個refresh page的button
使用方法:
只需要點擊相對應的圖案就可以使用想要的工具，然後就可以在右邊的畫布上作畫!
### Function description
Pencil
    點擊pencil圖案後就可以在右邊canvas上畫圖然後下方的
 可以調整brush大小及顏色

Eraser
  點擊橡皮擦圖案後就可以擦掉右邊的圖案
 也可以透過下方的工具來調整橡皮擦的大小
Clear
 點擊此圖案後canvas上的東西都會被清除
Text
點擊text圖案後在canvas點擊一下就會出現文字編輯框
Redo/Undo
點擊即可回到上一步/下一步
upload
 點擊此圖案就可以從電腦中找圖片來upload
Line/triangle/circle/rectangle
 
點擊個圖案後在canvas上就可以畫出相對應的shape



### Gitlab page link

    https://105000037.gitlab.io/AS_01_Webcanvas

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>
