var canvas, canvasGrid, canvasTmp, ctx, ctxGrid, ctxTmp;
var curX = 0, curY = 0, preX = -1, preY = -1;
var curColorBox, curColor = '#000000', curWidth = 2;

var flagShape = false, flagTool = false;
var currentTool = 'pencil';
var currentShape = '';
var currentShapeObj = null;
var font = '59px 微軟正黑體';
var font_name = 'Arial';
var hasInput = false;
var cPushArray = new Array();
var cStep = -1;
var isDrawing, points = [ ], radius = 15;
var density = 50;
var clientX, clientY, timeout;

function cPush() {
  cStep++;
  if (cStep < cPushArray.length) { cPushArray.length = cStep; }
  cPushArray.push(document.getElementById('canvas').toDataURL());
}
function clearpad() {
  var canvas = document.querySelector('#canvas');
  var ctx = canvas.getContext("2d");
  ctx.clearRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  var canvasTmp = document.querySelector('#canvasTmp');
  var ctxTmp = canvas.getContext("2d");
  ctxTmp.clearRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  ctxTmp.fillStyle = "white";
  ctxTmp.fillRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  var canvasGrid = document.querySelector('#canvas');
  var ctxGrid = canvas.getContext("2d");
  ctxGrid.clearRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  ctxGrid.fillStyle = "white";
  ctxGrid.fillRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);

}
function chooseTool(name) {
  currentTool = name;
  currentShape = '';
  canvasTmp.style.display = 'none';
  var link = document.getElementById(name).src;
  $('canvas').css('cursor', 'url(' + link + '), auto');
  hasInput = true;
  if(currentTool=='text'){
    hasInput=false;
    canvas.onclick = function (e) {
      if (hasInput) return;
      addInput(e.clientX, e.clientY);
    }
  }
  else if(currentTool=='undo'){
    if (cStep > 0) {
      cStep--;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
    
  }
  else if(currentTool=='redo'){
    if (cStep < cPushArray.length - 1) {
      cStep++;
      var canvasPic = new Image();
      canvasPic.src = cPushArray[cStep];
      canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
  }
  else if(currentTool=='neighbor'){
    ctx.lineJoin = ctx.lineCap = 'round';
  }
  else if(currentTool =='Gradient'){
    ctx.lineJoin = ctx.lineCap = 'round';
  }
}
function chooseShape(name) {
  ctxTmp.clearRect(0, 0, w, h);
  ctxTmp.beginPath();

  if (currentShapeObj != null) {
    currentShapeObj.draw(ctx);
  }

  currentTool = '';
  currentShape = name;
  canvasTmp.style.display = 'block';
  var link = document.getElementById(name).src;
  $('canvas').css('cursor', 'url(' + link + '), auto');
}

function chooseColor(id) {
  $('#color-quick-1').removeClass('active');
  $('#color-quick-2').removeClass('active');
  $('#' + id).addClass('active');

  curColorBox = document.getElementById(id);
  curColor = curColorBox.style.background;
}

function pickColor(id) {
  curColor = id;
  curColorBox.style.background = id;
}

function changeThickness(value) {
  curWidth = Math.round(parseInt(value) / 10);
  if (curWidth == 0)
    curWidth = 1;
  document.getElementById('rangeDemo').style.height = curWidth + 'px';
  document.getElementById('rangeValue').innerHTML = curWidth + 'px';
}
function addInput(x, y) {

  var input = document.createElement('input');
  font = curWidth * 10 + 'px ' + font_name;
  input.type = 'text';
  input.placeholder='text';
  input.style.position = 'fixed';
  input.style.left = (x - 4) + 'px';
  input.style.top = (y - 4) + 'px';
  input.style.font = font;
  input.style.color = curColor.color;
  input.onkeydown = handleEnter;
  document.body.appendChild(input);
  input.focus();
  hasInput = true;
}

function handleEnter(e) {
  var keyCode = e.keyCode;
  if (keyCode === 13) {
    drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
    document.body.removeChild(this);
    hasInput = false;
  }
}

function drawText(txt, x, y) {
  ctx.textBaseline = 'top';
  ctx.textAlign = 'left';
  ctx.fillStyle = curColor;
  ctx.font = font;
  ctx.fillText(txt, x-350, y - 130);
  cPush();
}
function change_font(f) {
  if (f == 0) {
    font_name = 'Arial';
  } else if (f == 1) {
    font_name = 'Verdana';
  } else if (f == 2) {
    font_name = 'Courier New';
  } else if (f == 3) {
    font_name = 'serif';
  } else if (f == 4) {
    font_name = 'sans-serif';
  }
  else if (f == 5) {
    font_name = '微軟正黑體';
  }
  else if (f == 6) {
    font_name = '新細明體';
  }
  else if (f == 7) {
    font_name = '標楷體';
  }
}
function init() {
  canvas = document.getElementById('canvas');
  canvasGrid = document.getElementById('canvasGrid');
  canvasTmp = document.getElementById('canvasTmp');

  canvas.width = 1000;
  canvas.height = 600;
  canvasGrid.width = canvas.width;
  canvasGrid.height = canvas.height;
  canvasTmp.width = canvas.width;
  canvasTmp.height = canvas.height;

  ctx = canvas.getContext('2d');
  ctxGrid = canvasGrid.getContext('2d');
  ctxTmp = canvasTmp.getContext('2d');

  w = canvas.width;
  h = canvas.height;
  
  canvas.addEventListener('mousedown', function (e) { handleToolDown(e) }, false);
  canvas.addEventListener('mouseup', function (e) { handleToolUp(e) }, false);
  canvas.addEventListener('mousemove', function (e) { handleToolMove(e) }, true);

  canvasTmp.addEventListener('mousedown', function (e) { handleShapeDown(e) }, false);
  canvasTmp.addEventListener('mouseup', function (e) { handleShapeUp(e)}, false);
  canvasTmp.addEventListener('mousemove', function (e) { handleShapeMove(e) }, false);
  
  
  //Tmp layers
  canvasTmp.style.display = 'none';
  //Painting
  ctx.clearRect(0, 0, w, h);
  ctx.beginPath();
  //Grid
  ctxGrid.clearRect(0, 0, w, h);
  ctxGrid.beginPath();
  ctxGrid.rect(0, 0, w, h);
  ctxGrid.fillStyle = '#FFFFFF';
  ctxGrid.fill();
  document.getElementById("fileUpload").addEventListener("change",readImage,false);
  ctx.clearRect(0, 0, w, h);
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, w, h);
  cPush();
  
  //Cursor
  $('canvas').css('cursor', 'url(pencil.png), auto');
  //Color box
  curColorBox = document.getElementById('color-quick-1');
  
}
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function readImage(){
  canvas = document.getElementById('canvas');

  canvas.width = 1065;
  canvas.height = 600;
  canvasGrid.width = canvas.width;
  canvasGrid.height = canvas.height;
  canvasTmp.width = canvas.width;
  canvasTmp.height = canvas.height;

  ctx = canvas.getContext('2d');
  ctxGrid = canvasGrid.getContext('2d');
  ctxTmp = canvasTmp.getContext('2d');
  w = canvas.width;
  h = canvas.height;
  if(this.files&&this.files[0]){
    ctx.clearRect(0, 0,w, h);
    clearpad();
    var FR = new FileReader();
    FR.onload = function(event) {
      var img = new Image();
      img.addEventListener("load", function() {
        ctx.drawImage(img, 0, 0,w, h);
        cPush();
      });
      img.src = event.target.result;
    };       
    FR.readAsDataURL( this.files[0] );
  }
}